# Simple testing approach

## Motivation
This is an example of a reactive store, with the following objectives:
- Show a little of the magic behind reactive libraries
- Show the usefulness of reactivity
- Show how to use reactivity without any external library or framework.


## How to use it

### The reactive Store
It is defined in [`/lib/store.js`](https://gitlab.com/penascal-frontend/javascript/vanilla-reactive-store/-/blob/main/lib/store.js)
It exports a class with one property and two methods:
- Property `value` is used to set and get the value stored in the instance of `Store` object.
- Method `subscribe` is used to add a finction to the set of functions to execute when `value` is changed.
- Method `unsubscribe` is used to remove a finction from the set of functions to execute when `value` is changed.
