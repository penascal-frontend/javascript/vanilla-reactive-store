import { Store } from "./lib/store.js"

const counter = new Store(0)
counter.subscribe((count => document.getElementById('counter').innerText = count))

const change = cant => counter.value = counter.value + cant

const increase = () => change(1) 
const decrease = () => change(-1) 

document.getElementById('increase').addEventListener('click', increase)
document.getElementById('decrease').addEventListener('click', decrease)
